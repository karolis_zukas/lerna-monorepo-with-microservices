import React, { useState, useEffect, ReactElement } from 'react';
import axios from 'axios';
import { getService, callService } from '@remono/util';

const App: React.FC = (): ReactElement => {
  const [foo, setFoo] = useState('N/A');

  useEffect(() => {
    (async function fetchApiAndData() {
      const service = await getService('@remono/testApiService', '1.3.0');
      const res = await callService('get', service, 'api/foo');

      setFoo(res.foo);
    }());
  });

  return (
    <div className="App">
      <h1>Hello World</h1>
      <p>
        Server responded with:
        {foo}
      </p>
    </div>
  );
};

export { App };
