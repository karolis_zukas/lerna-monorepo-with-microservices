import axios from 'axios';

export const getService = async (name, version) => {
    //TODO: these parameters should be moved to service registry parameters, or env vars and imported from there;
    const service = await axios.get(`http://localhost:5001/find/${name.replace('/', '%2f')}/${version}`);
    return service.data;
}

export const callService = async (method, service, resourcePath) => {
    const { ip, port } = service;
    const result = await axios[method](`http://${ip}:${port}/${resourcePath}`);
    return result.data;
}

/* TODO:
    service can expose availible routes, so which can be reached via service registry
*/