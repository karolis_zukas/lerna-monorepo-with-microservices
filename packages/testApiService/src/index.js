import axios from 'axios';
import app from './app';

const config = require('../config')[process.env.NODE_ENV || 'development'];

const server = app.listen(0, (err) => {
  if (err) {
    console.error(`ERROR: ${err.message}`);
  } else {
    console.log(`${config.name} ${config.version} listening on random port`);
    const registerService = () => axios.put(`http://localhost:5001/register/${config.name.replace('/', '%2F')}/${config.version}/${server.address().port}`);
    const unRegisterService = () => axios.delete(`http://localhost:5001/register/${config.name.replace('/', '%2F')}/${config.version}/${server.address().port}`);

    registerService();
    // Reregiter the service, as serviceRegistry will drop it after 30 sec.
    const interval = setInterval(registerService, 20000);
    const cleanup = async () => {
      clearInterval(interval);
      await unRegisterService();
    };

    process.on('uncaughtException', async () => {
      await cleanup();
      process.exit(0);
    });

    process.on('SIGINT', async () => {
      await cleanup();
      process.exit(0);
    });

    process.on('SIGTERM', async () => {
      await cleanup();
      process.exit(0);
    });
  }
});

app.on('listen', () => {
  console.log('test');
});
