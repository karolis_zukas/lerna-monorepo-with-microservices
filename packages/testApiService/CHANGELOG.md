# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.3.0](https://bitbucket.org/karolis_zukas/remono/compare/v1.2.0...v1.3.0) (2020-01-20)


### Features

* splitting to microservices ([1644004](https://bitbucket.org/karolis_zukas/remono/commits/1644004bc92a38eeec9907eee07f81a896c719ac))
