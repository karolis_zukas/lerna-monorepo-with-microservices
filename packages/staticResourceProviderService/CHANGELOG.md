# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.3.0](https://bitbucket.org/karolis_zukas/remono/compare/v1.2.0...v1.3.0) (2020-01-20)


### Features

* splitting to microservices ([1644004](https://bitbucket.org/karolis_zukas/remono/commits/1644004bc92a38eeec9907eee07f81a896c719ac))





# [1.2.0](https://bitbucket.org/karolis_zukas/remono/compare/v1.1.0...v1.2.0) (2020-01-18)


### Features

* add typescript to web ([69db011](https://bitbucket.org/karolis_zukas/remono/commits/69db011a3c908787a4f8a410a7d5ddd4442a3a70))





# [1.1.0](https://bitbucket.org/karolis_zukas/remono/compare/v0.1.1...v1.1.0) (2020-01-18)


### Features

* added yarn workspaces ([e616491](https://bitbucket.org/karolis_zukas/remono/commits/e61649175203f9675e87344bdd268f5450c22228))





## 0.1.1 (2020-01-18)

**Note:** Version bump only for package back
