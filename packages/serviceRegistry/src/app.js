import express from 'express';
import bodyParser from 'body-parser';
import ServiceRegistry from './lib/ServiceRegistry';

const app = express();
const serviceRegistry = new ServiceRegistry();
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.put('/register/:servicename/:serviceversion/:serviceport', (req, res) => {
  const { servicename, serviceversion, serviceport } = req.params;
  const serviceip = req.connection.remoteAddress.includes('::') ? `[${req.connection.remoteAddress}]` : req.connection.remoteAddress;
  const serviceKey = serviceRegistry
    .register(servicename, serviceversion, serviceip, serviceport);

  return res.json({ result: serviceKey });
});

app.delete('/register/:servicename/:serviceversion/:serviceport', (req, res) => {
  const { servicename, serviceversion, serviceport } = req.params;
  const serviceip = req.connection.remoteAddress.includes('::') ? `[${req.connection.remoteAddress}]` : req.connection.remoteAddress;
  const serviceKey = serviceRegistry
    .unregister(servicename, serviceversion, serviceip, serviceport);

  return res.json({ result: serviceKey });
});

app.get('/find/:servicename/:serviceversion', (req, res) => {
  const { servicename, serviceversion } = req.params;
  const svc = serviceRegistry.get(servicename, serviceversion);

  if (!svc) {
    return res.status(404).json({ result: 'Service not found' });
  }

  return res.json(svc);
});

module.exports = app;
