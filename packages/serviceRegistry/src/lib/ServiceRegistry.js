import semver from 'semver';

class ServiceRegistry {
  constructor() {
    this.services = {};
    this.timeout = 30;
  }

  get(name, version) {
    this.cleanup();
    const candidates = Object
      .values(this.services)
      .filter((service) => service.name === name && semver.satisfies(service.version, `>=${version}`));

    // return random service instance for now. TODO: add load balancing here;
    const randomCandidate = Math.floor(Math.random() * candidates.length);
    return candidates[randomCandidate];
  }

  register(name, version, ip, port) {
    this.cleanup();
    const key = name + version + ip + port;

    if (!this.services[key]) {
      this.services[key] = {};
      this.services[key].name = name;
      this.services[key].timestamp = Math.floor(new Date() / 1000);
      this.services[key].ip = ip;
      this.services[key].port = port;
      this.services[key].version = version;
      return key;
    }
    this.services[key].timestamp = Math.floor(new Date() / 1000);
    console.log(`Registered: ${name}/${version} on ${ip}:${port}`);
    return key;
  }

  unregister(name, version, ip, port) {
    const key = name + version + ip + port;

    console.log(`Unregistered: ${name}/${version} on ${ip}:${port}`);
    delete this.services[key];
    return key;
  }

  cleanup() {
    const now = Math.floor(new Date() / 1000);
    Object.keys(this.services).forEach((key) => {
      if (this.services[key].timestamp + this.timestamp < now) {
        // remove expired service;
        // TODO: add logging
        delete this.services[key];
      }
    });
  }
}

export default ServiceRegistry;
