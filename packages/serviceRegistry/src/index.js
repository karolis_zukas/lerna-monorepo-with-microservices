import app from './app';

const port = process.env.PORT || 5001;

app.listen(port, '0.0.0.0', (err) => {
  if (err) {
    console.error(`ERROR: ${err.message}`);
  } else {
    console.log(`Listening on port ${port}`);
  }
});
